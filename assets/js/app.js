'use strict';

function doScrolling(elementY, duration) {
    var startingY = window.pageYOffset;
    var diff = elementY - startingY;
    var start;

    // Bootstrap our animation - it will get called right before next frame shall be rendered.
    window.requestAnimationFrame(function step(timestamp) {
        if (!start) start = timestamp;
        // Elapsed miliseconds since start of scrolling.
        var time = timestamp - start;
        // Get percent of completion in range [0, 1].
        var percent = Math.min(time / duration, 1);

        window.scrollTo(0, startingY + diff * percent);

        // Proceed with animation as long as we wanted it to.
        if (time < duration) {
            window.requestAnimationFrame(step)
        }
    })
}

function getAnchorLinks() {
    var links = document.querySelectorAll('a'),
        anchorLinks = [];

    for (var i = 0; i < links.length; i++) {
        if (links[i].href.includes("#")) {
            anchorLinks.push(links[i]);
        }
    }

    return anchorLinks;
}

function getOffsetTop(elementId) {
    var element = elementId.replace('#', '');
    return document.getElementById(element).offsetTop;
}

function getElementBottomY(elementId) {
    var element = elementId.replace('#', ''),
        bottomY = document.getElementById(element).offsetHeight + document.getElementById(element).offsetTop;

    return bottomY;

}

(function setAnchorScroll() {
    var theAnchorLinks = getAnchorLinks();

    for (var i = 0; i < theAnchorLinks.length; i++) {

        theAnchorLinks[i].addEventListener("click", function (event) {
            event.preventDefault();
            var toScrollPosition = getOffsetTop(this.hash) - 70;
            doScrolling(toScrollPosition, 400)
        })
    }
})();


var topPositions = [];

(function getMenuItems(array) {
    var menuItems = document.querySelectorAll(".story-nav a");

    for (var i = 0; i < menuItems.length; i++) {
        if (menuItems[i].hash.includes("#")) {
            array.push(menuItems[i]);
        }
    }
})(topPositions);

function setActiveMenuItem(array) {
    for (var i = 0; i < topPositions.length; i++) {
        var elementOffsetTop = getOffsetTop(array[i].hash) - 70,
            elementBottom = getElementBottomY(array[i].hash) - 71;

        if (scrollPos >= elementOffsetTop && scrollPos <= elementBottom) {
            array[i].classList.add('active');
        } else if (array[i].classList.contains('active')) {
            array[i].classList.remove('active');
        }
    }
}

var scrollPos = 0;
window.addEventListener('scroll', function (e) {
    scrollPos = window.scrollY;

    window.requestAnimationFrame(function () {
        setActiveMenuItem(topPositions);
    });
});

var clipboard = new Clipboard('.clipboard');

clipboard.on('success', function(e) {
    var text = e.trigger.text;
    e.trigger.text = "copied!";

    setTimeout(function () {
        e.trigger.text = text;
    }, 2000);
});

clipboard.on('error', function(e) {
    var text = e.trigger.text;
    e.trigger.text = "error!";

    setTimeout(function () {
        e.trigger.text = text;
    }, 2000);
});

if(document.querySelector('.slider')) {

    var images = new Object({
        start: 0,
        size: 1,
        images: [],
        length: 0,
        availability: {
            previous: true,
            next: true
        },

        init: function() {
            this.images = document.querySelectorAll('.slider-content img');
            this.length = this.images.length;
            this.checkAvailability();

            // var mobileCase = document.querySelector(".slider-content").classList.contains("case-mobile");
            // if (mobileCase) {
            //     this.size = 3;
            // }

            this.renderSlideIndicators();
            this.render();
        },

        checkAvailability: function() {
            if(this.start + this.size === this.size) {
                this.availability.previous = false;
                document.querySelector('.previous').classList.add("disabled");
            } else {
                this.availability.previous = true;
                document.querySelector('.previous').classList.remove("disabled");
            }

            if(this.start + this.size === this.images.length) {
                this.availability.next = false;
                document.querySelector('.next').classList.add("disabled");
            } else {
                this.availability.next = true;
                document.querySelector('.next').classList.remove("disabled");
            }
        },

        renderSlideIndicators: function() {
            var wrapper = document.querySelector(".slider-indicator-wrapper");

            for(var i = 0; i < this.length; i++) {
                wrapper.innerHTML += '<div class="slider-indicator indicator-'+ i +' "></div>';
            }
        },

        setSlideIndicator: function () {
            var sliderIndicators = document.querySelectorAll(".slider-indicator");

            for(var i = 0; i < sliderIndicators.length; i++) {
                sliderIndicators[i].classList.remove("active");
            }

            sliderIndicators[this.start].classList.add("active");
        },

        render: function () {
            this.checkAvailability();

            var start = this.start,
                end = this.start + this.size;

            document.querySelector(".slider-content").innerHTML = "";

            for (var i = start; i < end; i++) {
                if(this.images[i] !== undefined) {

                    var imagePath = this.images[i].getAttribute('src').replace(".jpg", "");

                    document.querySelector(".slider-content").innerHTML += '<figure><img draggable="false" src="' + imagePath + '.png" srcset="' + imagePath + '.jpg 1x, ' + imagePath + '@2x.jpg 2x" id="slider-img-' + i + '"></figure>'
                }
            }

            this.setSlideIndicator();
        },

        next: function () {
            if(this.availability.next) {
                this.start = this.start + this.size;
                this.render();
                this.scrollTop();
            }
        },

        previous: function () {
            if(this.availability.previous) {
                this.start = this.start - this.size;
                this.render();
                this.scrollTop();
            }
        },

        scrollTop: function () {

            var toScrollPosition = getOffsetTop("slider-img-" + this.start) - 88;

            setTimeout(function() {
                doScrolling(toScrollPosition, 400)
            }, 120);

        }
    });

    images.init();

    var sliderIndicators = document.querySelectorAll(".slider-indicator");
    for(var i = 0; i < sliderIndicators.length; i++) {
        sliderIndicators[i].addEventListener("click", function(e){

            var classAray = this.className.split(' '),
                id = classAray[1].replace('indicator-', '');

            id = +id;

            images.start = id;
            images.render();
            images.scrollTop();
        });
    }

    document.querySelector(".next").addEventListener("click", function(e){
        images.next();
    });

    document.querySelector(".previous").addEventListener("click", function(e){
        images.previous();
    });


    var slider = new Hammer(document.querySelector('.slider'));

    slider.on("swipeleft swiperight", function(ev) {

        if(ev.type === "swipeleft") {
            images.next();
        } else if (ev.type === "swiperight"){
            images.previous();
        }
    });

}