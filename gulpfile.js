'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const twig = require('gulp-twig');
const concat = require('gulp-concat');
const uglifycss = require('gulp-uglifycss');
const uglify = require('gulp-uglify');
const pump = require('pump');
const imagemin = require('gulp-imagemin');
const htmlbeautify = require('gulp-html-beautify');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence');
const imageResize = require('gulp-image-resize');
const rename = require("gulp-rename");
const parallel = require("concurrent-transform");
const os = require("os");

var siteData = {
    nav: [
        {
            title: "Home",
            url: ""
        }, {
            title: "Work",
            url: "work"
        }, {
            title: "Contact",
            url: "contact"
        }
    ],
    timeline: [
        {
            date: "August 2011",
            text: "Started studying media design at ROC van Twente",
            icon: "small-books.svg"
        }, {
            date: "August 2013",
            text: "Internship at Recognize as front-end developer",
            icon: "code.svg"
        }, {
            date: "January 2014 - Now",
            text: "Started working at Recognize as front-end developer and designer",
            icon: "frontend-design.svg"
        }, {
            date: "May 2015",
            text: "Graduated as media designer",
            icon: "student.svg"
        }, {
            date: "September 2015 - Now",
            text: "Started studying Communication and Multimedia design at the Amsterdam University of Applied Sciences",
            icon: "books.svg"
        }, {
            date: "April 2017 - June 2017",
            text: "Design internship at MediaMonks",
            icon: "design.svg"
        }
],
    cases: {
        spacerace: {
          id: 0,
          title: "SpaceRace",
          slug: "work/spacerace",
          roles: ["designer"],
          url: "https://framer.cloud/wkdqr",
          btn_text: "Framer prototype (best viewed on iPhone)",
          lead: "This app aims to decrease the problem of getting distracted to much. It does that by letting the user collect tokens and upgrading his spaceship using these. The tokens can be collected by keeping the app open or the device locked and not using any other apps.\n" +
          "\n" +
          "We also included an option to invite others. The user will play as a team with these people. The goal is to collect the tokens together, if one person of the team gets distracted the whole team can’t collect tokens anymore and they ideally should correct the behaviour (in person).",
            base: "assets/img/spacerace/",
            images: [
                {
                    featured: true,
                    filename: "spacerace-1",
                    extension: ".png"
                }, {
                    featured: true,
                    filename: "spacerace-2",
                    extension: ".png"
                }, {
                    featured: true,
                    filename: "spacerace-3",
                    extension: ".png"
                }, {
                    featured: true,
                    filename: "spacerace-4",
                    extension: ".png"
                }, {
                    featured: true,
                    filename: "spacerace-5",
                    extension: ".png"
                }
            ]
        },
        imdb: {
            id: 1,
            title: "IMDb redesign",
            slug: "work/imdb",
            type: "imdb",
            roles: ["Designer"],
            url: "//framer.cloud/AZmcA/",
            btn_text: "Framer prototype",
            lead: 'As design exercise I made an unofficial redesign of the IMDb homepage.',
            base: "assets/img/imdb/",
            images: [
                {
                    featured: true,
                    filename: "imdb-1",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "imdb-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "imdb-3",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "imdb-featured",
                extension: ".jpg"
            }
        },
        csr_platform: {
            id: 2,
            title: "CSR report platform",
            slug: "work/csr-platform",
            type: "desktop",
            roles: ["Designer"],
            url: "/prototypes/csr-report/index.html",
            btn_text: "Prototype",
            lead: 'For VolkerWessels we are making a platform that helps collecting data to generate a CSR report. VolkerWessels is a big company with many subsidiary companies. All these companies have data that VolkerWessels needs to collect to make a complete CSR report.',
            base: "assets/img/csr_platform/",
            images: [
                {
                    featured: true,
                    filename: "csr-1",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "csr-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "csr-3",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "csr-featured",
                extension: ".jpg"
            },
            story: true,
            chapters: [
                {
                    title: "Charts",
                    paragraphs: ["The platform contains a lot of charts that are made by admins. These charts are getting assigned to an (underlying) company. These companies make charts that are connected to the chart they just got assigned, and can assign them to another company or just fill in the data themselves. This results in a tree of charts that contains a lot of chunked data, this way it is easy to manage. See the image below to get a view of this tree."],
                }, {
                    title: "Alarms",
                    paragraphs: ["Users can also make alarms that reminds people to fill in their data on time. These alarms are added to an individual meter and are inherited by the child meters."],
                }, {
                    title: "Reports",
                    paragraphs: ["Users that have the right permissions can generate detailed reports of the data."]
                    // media: {
                    //     type: "dribbble",
                    //     images: [
                    //         {
                    //             filename: "shot",
                    //             extension: ".png",
                    //             url: "//dribbble.com/shots/3215219-Doors-windows"
                    //         }
                    //     ]
                    // }
                }
            ]
        },
        bouwapp: {
            id: 3,
            title: "BouwApp portal",
            slug: "work/bouwapp",
            type: "desktop",
            roles: ["Designer"],
            url: "/prototypes/bouwapp/index.html",
            btn_text: "Prototype",
            lead: 'For the BouwApp I designed the portal that is being used by the clients of BouwApp. The portal is used to add projects to the BouwApp.</p><p class="lead">The BouwApp is a (Dutch) app that informs people that live around a building site about the current status of a project. They can subscribe to a project to receive notifications about it that can contain announcements about, for example, a street that is closed for some days.',
            base: "assets/img/bouwapp/",
            images: [
                {
                    featured: false,
                    filename: "bouwapp-1",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "bouwapp-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "bouwapp-3",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "bouwapp-4",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "bouwapp-featured",
                extension: ".jpg"
            }
        },
        opstap: {
            id: 4,
            title: "OpStap",
            slug: "work/opstap",
            type: "desktop",
            roles: ["Designer & front-end developer"],
            url: "//opstap.jelle.im",
            btn_text: "Demo",
            lead: 'OpStap (working name) is a side project of mine that I stopped working on. The idea was that groups of people could request a bus ride to a (music)festival using the platform. When the ride was requested, bus companies should be able to “claim” passengers by clusters. This way groups of people don’t get separated from each other.\n' +
            '</p><p class="lead">' +
            'The screens below are early designs of the bus company interface.' +
            '</p><p class="lead">' +
            'I stopped working on this project because the scope was just too big to get a hold on it while also studying and working. Nevertheless I think it is worth sharing.\n',
            base: "assets/img/opstap/",
            images: [
                {
                    featured: false,
                    filename: "opstap-3",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "opstap-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "opstap-1",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "opstap-featured",
                extension: ".jpg"
            }
        },
        /* kruidvatfr: {
            id: 4,
                title: "Kruidvat.fr",
                slug: "work/kruidvat-fr",
                type: "desktop",
                roles: ["Front-end developer"],
                url: "//kruidvat.fr/",
                btn_text: "Live website",
                lead: 'For <a href="//aswatson.com" target="_blank">A.S. Watson</a> we made the corporate website of Kruidvat France. The website is build using <a href="//twig.sensiolabs.org" target="_blank">Twig</a> and <a href="//symfony.com" target="_blank">Symfony</a>. I was tasked with coding a part of the templates and styling them. The styling of the homepage is almost identical to the Dutch Kruidvat site. The main purpose of the site is to let the (potential) customers get comfortable with the new drugstore.',
                base: "assets/img/kruidvat/",
                images: [
                {
                    featured: true,
                    filename: "kruidvat-1",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "kruidvat-2",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "kruidvat-featured",
                extension: ".jpg"
            }
        },
        cabin_configurator: {
            id: 5,
                title: "Cabin configurator",
                slug: "work/cabin-configurator",
                type: "desktop",
                roles: ["Designer", "Front-end developer"],
                url: "//lugarde-configurator.nl/",
                btn_text: "Live website",
                lead: '<a href="//lugarde.nl/" target="_blank">Lugarde</a> came to <a href="//recognize.nl/" target="_blank">Recognize</a> and asked if we could make a HTML5 application that enables the user to configure their own cabin. When the user submits the cabin it has to send the configuration to the nearest Lugarde dealer. In some cases the user can also order the entire configuration and send it to their house.',
                base: "assets/img/lugarde/",
                images: [
                {
                    featured: false,
                    filename: "lugarde-1",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "lugarde-2",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "lugarde-3",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "lugarde-4",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "lugarde-5",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "lugarde-6",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "lugarde-7",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "lugarde-featured",
                extension: ".jpg"
            },
                story: true,
                chapters: [
                {
                    title: "Research",
                    paragraphs: ["I started this project by searching a lot of comparable product configurators as example. The best examples that I found were car configurators. After this I started pointing out what the average target audience is. It was clear that this project was going to be a hard one. Mainly because the primary audience were people from 40 till about 60 years old and they didn’t have very much experience in using the internet."]
                }, {
                    title: "Sketches",
                    paragraphs: ["As with most of my designs I started with making some quick sketches to get some ideas on paper and make it a bit more tangible. Here are some of the sketches I made for this project."],
                    media: {
                        type: "normal",
                        images: [
                            {
                                filename: "sketch-1-thumb",
                                extension: ".jpg"
                            }, {
                                filename: "sketch-2-thumb",
                                extension: ".jpg"
                            }
                        ]
                    }
                }, {
                    title: "Iterations",
                    paragraphs: ["Here you see three different major versions of the cabin configurator."],
                    media: {
                        type: "normal",
                        images: [
                            {
                                filename: "version-1-thumb",
                                extension: ".jpg"
                            }, {
                                filename: "version-2-thumb",
                                extension: ".jpg"
                            }, {
                                filename: "version-3-thumb",
                                extension: ".jpg"
                            }
                        ]
                    }
                }, {
                    title: "Product icons",
                    paragraphs: ["To give the user an optimal work and viewing space I decided to make icons for each product that the user can choose. The icons are optimized to fit in the product bar of the application. Squeezing images into the bar would force us to include a lightbox-like feature and the user then needs to open an image before clearly seeing the product. Below are some of the icons I made for this project."],
                    media: {
                        type: "dribbble",
                        images: [
                            {
                                filename: "shot",
                                extension: ".png",
                                url: "//dribbble.com/shots/3215219-Doors-windows"
                            }
                        ]
                    }
                }
            ]
        },
        payperuse: {
            id: 6,
            title: "IoT pay-per-use device app",
            slug: "work/pay-per-use",
            type: "case-mobile",
            roles: ["Designer"],
            url: "https://framer.cloud/goetK",
            btn_text: 'Framer prototype',
            lead: 'This is a simple app concept that monitors a pay-per-use washing machine concept. The user gets a quick overview of their costs.',
            base: "assets/img/pay-per-use/",
            images: [
                {
                    featured: true,
                    filename: "pay-per-use-1",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "pay-per-use-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "pay-per-use-3",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "pay-per-use-featured-mobile",
                extension: ".jpg"
            }
        },
        planetenpad: {
            id: 7,
            title: "Planetenpad",
            slug: "work/planeten-pad",
            type: "case-mobile",
            roles: ["Designer"],
            url: "https://framer.cloud/vhzjR/",
            btn_text: "Prototype",
            lead: 'An observatory asked Recognize to develop a concept and design an exploring application. My colleagues developed a concept and an prototype for an augmented reality app. The app is inspired on Pokémon Go. But instead of catching Pokémons the children are going to catch planets on a route from the center of a village to the observatory itself. From here on I designed the User Interface and I made a Framer prototype.',
            base: "assets/img/pp/",
            images: [
                {
                    featured: false,
                    filename: "pp-1",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "pp-2",
                    extension: ".jpg"
                }, {
                    featured: false,
                    filename: "pp-3",
                    extension: ".jpg"
                }
            ],
            featured_image: {
                filename: "pp-featured-mobile",
                extension: ".jpg"
            }
        },
        kidsfabrics: {
            id: 8,
            title: "KidsFabrics",
            slug: "work/kidsfabrics",
            type: "desktop",
            roles: ["Designer"],
            lead: 'For KidsFabrics I made this screen as a style proposal. KidsFabrics is, as the name describes, a company that sells fabrics meant for kids. Customers can order fabrics but also choose a fabric to wrap around, for example, a pillow.',
            base: "assets/img/kidsfabrics/",
            images: [
                {
                    featured: true,
                    filename: "kidsfabrics-1",
                    extension: ".jpg"
                }
            ]
        },
        vwt: {
            id: 9,
            title: "VWT support portal",
            slug: "work/vwt-support-portal",
            type: "desktop",
            roles: ["Designer & front-end developer"],
            lead: 'For VolkerWessels Telecom we are making a support portal that should reduce the amount of support tickets. I tried a style that is a bit different of what VolkerWessels Telecom usually uses. Unfortunately the style didn’t make it, the UX however is still the same.',
            base: "assets/img/vwt-support-portal/",
            images: [
                {
                    featured: true,
                    filename: "vwt-support-portal-1",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "vwt-support-portal-2",
                    extension: ".jpg"
                }, {
                    featured: true,
                    filename: "vwt-support-portal-3",
                    extension: ".jpg"
                }
            ]
        },*/
        // checklist_app: {
        //     id: 10,
        //     title: "Checklist app",
        //     slug: "work/checklist-app",
        //     type: "case-mobile",
        //     roles: ["Designer"],
        //     btn_text: "Prototype",
        //     lead: 'For VolkerWessels Telecom I designed an application that will be used by mechanics of the company. Before mechanics start working on a job they have to fill in a Last Minute Risk Analysis (LMRA) to make sure everything is safe. This app automatically fills in all required data that can be collected by a device. To ensure the mechanics can navigate quickly the buttons are big and positioned in reach of the thumb.',
        //     base: "assets/img/check/",
        //     images: [
        //         {
        //             featured: true,
        //             filename: "check-1",
        //             extension: ".png"
        //         }, {
        //             featured: true,
        //             filename: "check-2",
        //             extension: ".png"
        //         }, {
        //             featured: true,
        //             filename: "check-3",
        //             extension: ".png"
        //         }
        //     ]
        // }
    }
};

gulp.task('sass', function () {
    gulp.src('./assets/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./web/assets/css/'));
});

gulp.task('twigRoot', function () {
    var files = [
        "./assets/templates/index.twig",
        "./assets/templates/work.twig",
        "./assets/templates/contact.twig"
    ];

    return gulp.src(files)
        .pipe(twig({
            data: siteData
        }))
        .pipe(gulp.dest('./web/'));
});

gulp.task('twigWork', function () {
    var files = [
        "./assets/templates/cabin-configurator.twig",
        "./assets/templates/checklist-app.twig",
        "./assets/templates/imdb.twig",
        "./assets/templates/kruidvat-fr.twig",
        "./assets/templates/planeten-pad.twig",
        "./assets/templates/csr-platform.twig",
        "./assets/templates/opstap.twig",
        "./assets/templates/bouwapp.twig",
        "./assets/templates/kidsfabrics.twig",
        "./assets/templates/pay-per-use.twig",
        "./assets/templates/vwt-support-portal.twig",
        "./assets/templates/spacerace.twig"
    ];

    return gulp.src(files)
        .pipe(twig({
            data: siteData
        }))
        .pipe(gulp.dest('./web/work/'));
});

gulp.task('twig', ['twigRoot', 'twigWork']);

gulp.task('js', function () {
    return gulp.src('assets/js/*.js')
        .pipe(gulp.dest('./web/assets/js'));
});

gulp.task('depAssets', function () {
    var files = [
        "./bower_components/photoswipe/dist/default-skin/*"
    ];

    return gulp.src(files)
        .pipe(gulp.dest('./web/assets/css/'));
});

gulp.task('cssDeps', function () {
    var files = [
        "./bower_components/photoswipe/dist/photoswipe.css",
        "./bower_components/photoswipe/dist/default-skin/default-skin.css"
    ];

    return gulp.src(files)
        .pipe(concat('dependencies.css'))
        .pipe(gulp.dest('./web/assets/css'));
});

gulp.task('jsDeps', function () {
    var files = [
        "./bower_components/hammerjs/hammer.js",
        "./bower_components/clipboard/dist/clipboard.js"
    ];

    return gulp.src(files)
        .pipe(concat('dependencies.js'))
        .pipe(gulp.dest('./web/assets/js'));
});

gulp.task('htaccess', function () {
    return gulp.src('assets/.htaccess')
        .pipe(gulp.dest('./web/'));
});

gulp.task('img', function () {
    return gulp.src('assets/img/**/*')
        .pipe(gulp.dest('./web/assets/img'))
});

gulp.task('prototypes', function () {
    return gulp.src('assets/prototypes/**/*')
        .pipe(gulp.dest('./web/prototypes'))
});

gulp.task('watch', function () {
    gulp.watch('./assets/**/*', ['sass', 'twig', 'js']);
});

gulp.task('compress', function (cb) {
    gulp.src('./web/assets/css/*.css')
        .pipe(uglifycss({
            "cuteComments": true
        }))
        .pipe(gulp.dest('./web/assets/css'));

    pump([
            gulp.src('./web/assets/js/*.js'),
            uglify({}),
            gulp.dest('./web/assets/js')
        ],
        cb
    );
});

gulp.task('compressImg', function () {
    gulp.src('./web/assets/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./web/assets/img'));
});

gulp.task('beautify', function() {
    var options = {
        indentSize: 4,
        preserve_newlines: false
    };

    gulp.src('./web/*.html')
        .pipe(htmlbeautify(options))
        .pipe(gulp.dest('./web/'))
});

gulp.task('clean', function () {
    return gulp.src('web/*', {read: false})
        .pipe(clean());
});


const thumbWidth = 426;
const thumbHeight = 266;
gulp.task('makeThumbs', function () {
    gulp.src("assets/img/**/*-featured.{jpg,png}")
        .pipe(parallel(
            imageResize({
                width: thumbWidth,
                height : thumbHeight,
                gravity: 'North',
                crop: true,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(rename(function (path) { path.basename += "-thumb"; }))
        .pipe(gulp.dest('web/assets/img/'));
});

gulp.task('makeRetinaThumbs', function () {
    gulp.src("assets/img/**/*-featured.{jpg,png}")
        .pipe(parallel(
            imageResize({
                width: thumbWidth*2,
                height : thumbHeight*2,
                gravity: 'North',
                crop: true,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(rename(function (path) { path.basename += "-thumb@2x"; }))
        .pipe(gulp.dest('web/assets/img/'));
});

gulp.task('makeThumbsMobile', function () {
    gulp.src("assets/img/**/*-featured-mobile.{jpg,png}")
        .pipe(parallel(
            imageResize({
                //width: thumbWidth,
                height : thumbHeight,
                gravity: 'North',
                crop: false,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(rename(function (path) { path.basename += "-thumb"; }))
        .pipe(gulp.dest('web/assets/img/'));
});

gulp.task('makeRetinaThumbsMobile', function () {
    gulp.src("assets/img/**/*-featured-mobile.{jpg,png}")
        .pipe(parallel(
            imageResize({
                //width: thumbWidth*2,
                height : thumbHeight*2,
                gravity: 'North',
                crop: false,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(rename(function (path) { path.basename += "-thumb@2x"; }))
        .pipe(gulp.dest('web/assets/img/'));
});

gulp.task('thumbs', ['makeThumbs', 'makeRetinaThumbs', 'makeThumbsMobile', 'makeRetinaThumbsMobile']);
gulp.task('compile', ['sass', 'twig', 'jsDeps', 'cssDeps', 'depAssets', 'js']);
gulp.task('default', ['img', 'thumbs', 'htaccess', 'prototypes', 'compile']);
gulp.task('compressAll', ['compress', 'compressImg', 'beautify']);
gulp.task('prod', gulpSequence('clean', ['sass', 'twig', 'jsDeps', 'cssDeps', 'depAssets', 'js', 'img', 'thumbs', 'prototypes', 'htaccess'], ['compress', 'compressImg', 'beautify']));