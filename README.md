# Portfolio Jelle Overbeek - README #

Personal portfolio build using Twig and SCSS. 

## Installation ##

1. ```npm install``` 

2. ```bower install```

3. ```gulp```

4. The **/web** dir that is compiled by Gulp should be used as website root.

## Gulp commands ##

* ```gulp sass``` compiles .sass files into **/web/assets/css/style.css**.

* ```gulp js``` copies all .js files of **/assets/js** to **/web/assets/js**.

* ```gulp twig``` compiles all twig files of **/assets/templates** to HTML files **/web/**.

* ```gulp img``` copies all files of **/assets/img** to **/web/assets/img** .

* ```gulp cssDeps``` concats and  copies all CSS dependencies (from bower) to **/web/assets/css**.

* ```gulp jsDeps``` concats and copies all JS dependencies (from bower) to **/web/assets/js**

* ```gulp depAssets``` copies all dependency assets (from bower) to **/web/assets/css**
.
* ```gulp htaccess``` copies **.htaccess** file to **/web/**.

* ```gulp compile``` runs ```gulp sass```, ```gulp js``` and ```gulp twig```.

* ```gulp``` runs ```gulp img```, ```gulp htaccess``` and ```gulp compile```.

* ```gulp watch``` Watches all files into **/assets** and runs ```gulp compile``` when a file changes.

* ```gulp compress``` Minifies all CSS and JS files in **/web/**.

* ```gulp compressImg``` Compresses all images in **/web/**.

* ```gulp compressAll``` Runs ```gulp compress``` and ```gulp compressImg```.

* ```gulp beautify``` Beautifies all html in **/web/**.

* ```gulp clean``` Removes content of **/web/**.

* ```gulp prod``` Runs all gulp commands in correct order.